<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

//A post belongs to a user so that other user cannot edit the said post.
//$this -> refers to the current model.
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
