<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function hello(){
        //return view('hello')->with('name', 'Spongebob Sqaurepants');
        //return view ('hello', ['name' => 'Patrick Star']);

        $info = array(
            'frontend' => 'Zuitt Coding Bootcamp',
            'topics' => ['HTML', 'CSS', 'JS', 'react']
        );
        return view ('hello')->with($info);
    }

    public function about(){
        return view('./pages/about');
    }
    public function services(){
        return view('./pages/services');
    }
}
