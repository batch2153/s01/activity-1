@extends('layouts.app')

@section('content')
<h1>Posts<h/>
<br>
<br>
@if(count($posts) > 0)
    @foreach($posts as $post)
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">{{ $post->title}}</h5>
                    <a href="/posts/{{ $post->id}}">{{ $post->title}}</a>
                <h6>Written on: {{ $post->created_at}}</h6>
            </div>
        </div>
        <br>
    @endforeach
    {{ $posts->links() }}
@endif

@endsection